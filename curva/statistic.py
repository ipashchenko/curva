import george
import numpy as np


def lnlike(p, curve, t, y, yerr):
    """
    Likelihood of normal noise model.
    """
    curve.p = p
    return (-0.5 * np.log(2. * np.pi * yerr ** 2.) - (y - curve(t)) ** 2. / (2. * yerr ** 2.)).sum()


def lnlike_gp(p, curve, t, y, yerr):
    """
    Likelihood of GP noise model.
    """
    a, tau, d = np.exp(p[:3])
    gp = george.GP(a * george.kernels.Matern32Kernel(tau) +
                   george.kernels.WhiteKernel(d))
    gp.compute(t, yerr)
    curve.p = p[3:]
    return gp.lnlikelihood(y - curve(t))


def lnlike_gp_only(p, t, y, yerr):
    """
    Likelihood of GP noise model only.
    """
    a, tau, d = np.exp(p)
    gp = george.GP(a * george.kernels.Matern32Kernel(tau) +
                   george.kernels.WhiteKernel(d))
    gp.compute(t, yerr)
    return gp.lnlikelihood(y)


def lnlike_gp_only_rq(p, t, y, yerr):
    """
    Likelihood of GP noise model only.
    """
    a, tau, alpha, d = np.exp(p)
    gp = george.GP(a * george.kernels.RationalQuadraticKernel(alpha, tau) +
                   george.kernels.WhiteKernel(d))
    gp.compute(t, yerr)
    return gp.lnlikelihood(y)


def lnprior_gp(p, curve):
    """
    Prior of GP noise model.
    """
    lna, lntau, lnd = p[:3]
    if not -20 < lna < 3:
        return -np.inf
    if not -20 < lntau < 50:
        return -np.inf
    if not -10 < lnd < 3:
        return -np.inf
    return curve.lnpr(p[3:])


def lnprior_gp_only(p):
    """
    Prior of GP noise model only.
    """
    lna, lntau, lnd = p
    if not -20 < lna < 3:
        return -np.inf
    if not -20 < lntau < 50:
        return -np.inf
    if not -10 < lnd < 3:
        return -np.inf
    return 0.


def lnprior_gp_only_rq(p):
    """
    Prior of GP noise model only.
    """
    lna, lntau, lnalpha, lnd = p
    if not -20 < lna < 3:
        return -np.inf
    if not -20 < lntau < 50:
        return -np.inf
    if not -20 < lnalpha < 10:
        return -np.inf
    if not -20 < lnd < 3:
        return -np.inf
    return 0.


def lnprob_gp_only(p, t, y, yerr):
    """
    Posteriror probability of GP noise model only.
    :param p:
    :param y:
    :param yerr:
    :return:
    """
    lp = lnprior_gp_only(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike_gp_only(p, t, y, yerr)


def lnprob_gp_only_rq(p, t, y, yerr):
    """
    Posteriror probability of GP noise model only.
    :param p:
    :param y:
    :param yerr:
    :return:
    """
    lp = lnprior_gp_only_rq(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike_gp_only_rq(p, t, y, yerr)


def lnprior_gp_wo(p, curve):
    """
    Prior of GP noise model.
    """
    lna, lntau = p[:2]
    if not -20 < lna < 3:
        return -np.inf
    if not -20 < lntau < 50:
        return -np.inf
    return curve.lnpr(p[2:])


def lnprob(p, curve, t, y, yerr):
    """
    Posterior probability of normal noise model.
    """
    lp = curve.lnpr(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(p, curve, t, y, yerr)


def lnprob_gp(p, curve, t, y, yerr):
    """
    Posterior probability of GP noise model.
    """
    lp = lnprior_gp(p, curve)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike_gp(p, curve, t, y, yerr)
