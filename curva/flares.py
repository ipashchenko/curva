import numpy as np
from utils import FunctionWrapper, is_sorted


class Flare(object):
    """
    Basic class that implements single flare.
    """
    def __init__(self):
        self._p = None
        self._parnames = []
        self._fixed = np.array([], dtype=bool)
        self._lnprior = dict()
        self._prior_rvs = dict()
        self._prior_pars = dict()
        self._prior_func = dict()

    def __call__(self, t):
        raise NotImplementedError

    def add_prior(self, prior):
        """
        Add/update prior distribution for some parameters.
        :param prior:
            Kwargs with keys - name of the parameter and values - (callable,
            args, kwargs,) where args & kwargs - additional arguments to
            callable. Each callable is called callable.logpdf(p, *args,
            **kwargs).
        Example:
        {'amp': (scipy.stats.uniform, {'loc': 0., 'scale': 30},)}
        First key will result in calling: ``scipy.stats.uniform.logpdf(x,
        loc=0., scale=10.)`` as ln of prior pdf for ``flux`` parameter. In
        ``from_prior`` method this result in calling
        ``scipy.stats.uniform.rvs(loc=0., scale=10.)``.
        """
        for key, value in prior.items():
            if key in self._parnames:
                func, kwargs = value
                self._prior_func[key] = func
                self._prior_pars[key] = kwargs
                self._lnprior.update({key: FunctionWrapper(func.logpdf,
                                                           **kwargs)})
                self._prior_rvs.update({key: FunctionWrapper(func.rvs,
                                                             **kwargs)})
            else:
                raise Exception("Unknown parameter name: " + str(key))

    @property
    def p(self):
        """
        Shortcut for parameters of model.
        """
        return self._p[np.logical_not(self._fixed)]

    @p.setter
    def p(self, p):
        self._p[np.logical_not(self._fixed)] = p[:]

    @property
    def lnpr(self):
        if not self._lnprior.keys():
            raise Exception("No priors specified!")
        lnprior = list()
        for key, value in self._lnprior.items():
            p = self._p[self._parnames.index(key)]
            lnprior.append(value(p))
        return sum(lnprior)

    @property
    def from_prior(self):
        """
        Generate parameter vector from prior.
        :return:
            List of parameters generated from prior distributions.
        """
        if not self._prior_rvs.keys():
            raise Exception("No priors specified!")
        pars = dict()
        for key, value in self._prior_rvs.items():
            pars.update({key: value.rvs})
        return pars


class ConstBackground(Flare):
    def __init__(self, amp, fixed=None):
        super(ConstBackground, self).__init__()
        self._parnames.extend(['amp'])
        # TODO: Move up to ``Flare``
        self._fixed = np.concatenate((self._fixed,
                                      np.array([False]),))
        self._p = np.array([amp])
        self.size = 1
        # TODO: Add to method in ``Flare``
        if fixed is not None:
            for par in fixed:
                if par not in self._parnames:
                    raise Exception('Uknown parameter ' + str(par) + ' !')
                self._fixed[self._parnames.index(par)] = True

    def __call__(self, t):
        amp = self._p
        return amp * np.ones(len(t), dtype=float)


class LinearTrend(Flare):
    def __init__(self, k, b, fixed=None):
        super(LinearTrend, self).__init__()
        self._parnames.extend(['amp', 'scale'])
        # TODO: Move up to ``Flare``
        self._fixed = np.concatenate((self._fixed,
                                      np.array([False, False]),))
        self._p = np.array([k, b])
        self.size = 1
        # TODO: Add to method in ``Flare``
        if fixed is not None:
            for par in fixed:
                if par not in self._parnames:
                    raise Exception('Uknown parameter ' + str(par) + ' !')
                self._fixed[self._parnames.index(par)] = True

    def __call__(self, t):
        k, b = self._p
        return k * t + b


class GaussianFlare(Flare):
    def __init__(self, amp, loc, sig2, fixed=None):
        super(GaussianFlare, self).__init__()
        self._parnames.extend(['amp', 'loc', 'sig2'])
        self._fixed = np.concatenate((self._fixed,
                                      np.array([False, False, False]),))
        self._p = np.array([amp, loc, sig2])
        self.size = 3
        if fixed is not None:
            for par in fixed:
                if par not in self._parnames:
                    raise Exception('Uknown parameter ' + str(par) + ' !')
                self._fixed[self._parnames.index(par)] = True

    def __call__(self, t):
        amp, loc, sig2 = self._p
        return amp * np.exp(-0.5 * (t - loc) ** 2 / sig2)


class ExpFlare(Flare):
    def __init__(self, amp, loc, sig21, sig22, fixed=None):
        super(ExpFlare, self).__init__()
        self._parnames.extend(['amp', 'loc', 'sig21', 'sig22'])
        self._fixed = np.concatenate((self._fixed,
                                      np.array([False, False, False, False]),))
        self._p = np.array([amp, loc, sig21, sig22])
        self.size = 4
        if fixed is not None:
            for par in fixed:
                if par not in self._parnames:
                    raise Exception('Uknown parameter ' + str(par) + ' !')
                self._fixed[self._parnames.index(par)] = True

    def __call__(self, t):
        amp, loc, sig21, sig22 = self._p
        return np.hstack((abs(amp) * np.exp((t[t < loc] - loc)/sig21),
                          abs(amp) * np.exp(-(t[t >= loc] - loc)/sig22)))


class Curve(object):
    """
    Basic class that represents general functionality of flare models.
    """
    def __init__(self, freq=None):
        self._ln_hprior = dict()
        self._hprior_rvs = dict()
        self._hprior_pars = dict()
        self._hprior_func = dict()
        self._flares = list()
        self._parnames = list()

    @property
    def parnames(self):
        return self._parnames

    def __call__(self, t):
        return np.sum([flare(t) for flare in self._flares], axis=0)

    def __add__(self, other):
        self._flares.extend(other._flares)
        self._parnames.extend(other._parnames)

    def add_flare(self, flare):
        self._flares.append(flare)
        self._parnames.extend(flare._parnames)

    def add_flares(self, *flares):
        for flare in flares:
            self.add_flare(flare)

    def remove_flare(self, flare):
        self._flares.remove(flare)
        for par in flare._parnames:
            self._parnames.remove(par)

    def remove_flares(self, *flares):
        for flare in flares:
            self.remove_flare(flare)

    def clear_flares(self):
        self._flares = list()

    @property
    def p(self):
        """
        Shortcut for parameters of flares model.
        """
        p = list()
        for flare in self._flares:
            p.extend(flare.p)
        return p

    @p.setter
    def p(self, p):
        for flare in self._flares:
            flare.p = p[:flare.size]
            p = p[flare.size:]

    @property
    def size(self):
        return len(self.p)

    # FIXME: This should null any priors specified before
    def add_hyper_prior(self, hprior):
        """
        Add/update hyper prior distribution for some parameters.

        :param hprior:
            Dictionary with hyperpriors specification.

        Example:
            If ``amp`` of individual flares has lognormal prior distribution
            with parameters ``loc`` and ``scale`` then hyperprior for that
            parameters is defined by dictionary:

            {'amp':
                {'loc':
                    (scipy.stats.uniform, {'loc': 0., 'scale': 10.})}
        """
        # ``amp``: {``loc``: ...
        for key, value in hprior.items():
            if key in self.parnames:
                self._hprior_pars[key] = dict()
                self._ln_hprior[key] = dict()
                self._hprior_rvs[key] = dict()
                self._hprior_func[key] = dict()
                # ``loc``: scipy.stats.uniform, {'loc': 0., 'scale': 10}
                for key_, value_ in value.items():
                    func, kwargs = value_
                    self._hprior_func[key][key_] = func
                    self._hprior_pars[key][key_] = kwargs
                    self._ln_hprior[key].update({key_:
                                                     FunctionWrapper(func.logpdf,
                                                                     **kwargs)})
                    self._hprior_rvs[key].update({key_:
                                                     FunctionWrapper(func.rvs,
                                                                     **kwargs)})
            else:
                raise Exception("Unknown parameter name: " + str(key))

    def lnhpr(self, p):
        lnhpr = list()
        for par in self._hprior_pars.keys():
            for flare in self._flares:
                if par in flare._parnames:
                    for par_, par_value in flare._prior_pars[par].items():
                        lnhpr.append(self._ln_hprior[par][par_](par_value))
        return sum(lnhpr)

    def _is_sorted(self):
        locations = list()
        for flare in self._flares:
            if 'loc' in flare._parnames:
                locations.append(flare.p[flare._parnames.index('loc')])
        return is_sorted(locations)

    def lnpr(self, p):
        self.p = p[:]
        if not self._is_sorted():
            return -np.inf
        lnpr = list()
        for flare in self._flares:
            lnpr.append(flare.lnpr)
        return sum(lnpr)

    # TODO: This changes priors of individual flares.
    @property
    def from_hprior(self):
        if self._hprior_rvs.keys():
            # Cycle for hyperparameters
            # ``amp``, {``loc``: ..., ``scale``:, ...}
            for key, value in self._hprior_rvs.items():
                # Cycle for components with this hyperparameter & update prior
                for flare in self._flares:
                    # ``amp``
                    if key in flare._parnames:
                        hpars = dict()
                        # ``loc``, FuncWrapper
                        # ``scale``, FuncWrapper
                        for key_, value_ in value.items():
                            upd = value_.rvs
                            hpars.update({key_: upd})
                        flare.add_prior({key: (flare._prior_func[key], hpars)})
                        print "Set new prior for flare ", hpars
        else:
            raise Exception("No hyper prior specified")
            return None
        return hpars

    @property
    def from_prior(self):
        """
        :return:
            List of dictionaries with flares parameters.
        """
        pars = list()
        for flare in self._flares:
                pars.append(flare.from_prior)
        return pars
