import numpy as np
import emcee
import matplotlib.pyplot as plt
import george
from scipy import stats
from flares import ConstBackground, GaussianFlare, Curve, ExpFlare
from statistic import lnprob_gp


# Use .toordinal() or .strftime("%s") for datetime
if __name__ == "__main__":
    t, y, yerr = np.loadtxt('/home/ilya/Dropbox/Ilya/sim_lc2.dat', unpack=True)
    import pickle
    with open('/home/ilya/code/gp_curves/gp_curves/out.pkl', 'rb') as fo:
        data = pickle.load(fo)
    t1, y1, yerr1, t2, y2, yerr2 = data
    t1 = np.array([t.toordinal() for t in t1])
    t2 = np.array([t.toordinal() for t in t2])
    t1_0 = t1[0]
    t2_0 = t2[0]
    t1 = t1 - t1_0
    t2 = t2 - t2_0
    y1 = np.array(y1, dtype='float')
    y2 = np.array(y2, dtype='float')
    yerr1 = np.array(yerr1, dtype='float')
    yerr2 = np.array(yerr2, dtype='float')
    plt.figure()
    plt.errorbar(t1, y1, yerr1, fmt='.k')
    plt.errorbar(t2, y2, yerr2, fmt='.r')

    # Choose flares
    # indx1 = np.logical_and(t1 > 5932, t1 < 7760)
    indx1 = np.logical_and(t2 > 5932, t2 < 7760)
    t = t1[indx1]
    t = t - t[0]
    y = y1[indx1]
    yerr = yerr1[indx1]
    plt.figure()
    plt.errorbar(t, y, yerr, fmt='.k')

    # # To fit only part of data mask it
    # mask = np.logical_and(0 < t, t < 10000)
    # t = t[mask]
    # y = y[mask]
    # yerr = yerr[mask]

    # Initialize Curve object
    curve = Curve()
    bg = ConstBackground(0.5)
    bg.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 2.})})
    fl1 = ExpFlare(7., 280., 100., 300.)
    fl1.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 10.}),
                   'loc': (stats.uniform, {'loc': 0., 'scale': t[-1]}),
                   'sig21': (stats.uniform, {'loc': 10., 'scale': 400.0}),
                   'sig22': (stats.uniform, {'loc': 10., 'scale': 400.0})})
    fl2 = ExpFlare(2., 950., 100., 300.)
    fl2.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 5.}),
                   'loc': (stats.uniform, {'loc': 0., 'scale': t[-1]}),
                   'sig21': (stats.uniform, {'loc': 10., 'scale': 400.0}),
                   'sig22': (stats.uniform, {'loc': 10., 'scale': 400.0})})
    # fl3 = ExpFlare(2., 540., 100., 300.)
    # fl3.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 5.}),
    #                'loc': (stats.uniform, {'loc': 0., 'scale': t[-1]}),
    #                'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
    #                'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    # fl4 = ExpFlare(3., 900., 100., 300.)
    # fl4.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 5.}),
    #                'loc': (stats.uniform, {'loc': 0., 'scale': t[-1]}),
    #                'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
    #                'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    # fl5 = ExpFlare(0.5, 1050., 100., 300.)
    # fl5.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 5.}),
    #                'loc': (stats.uniform, {'loc': 0., 'scale': t[-1]}),
    #                'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
    #                'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    # fl1 = GaussianFlare(np.max(y), 1000., 500000.)
    # fl1.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #               'loc': (stats.uniform, {'loc': 0., 'scale': 1700.}),
    #               'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl2 = GaussianFlare(np.max(y), 3000., 500000.)
    # fl2.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #               'loc': (stats.uniform, {'loc': 1700., 'scale': 4000.}),
    #               'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl3 = GaussianFlare(np.max(y), 5000., 500000.)
    # fl3.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 4000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl4 = GaussianFlare(np.max(y), 7000., 500000.)
    # fl4.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 6000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl5 = GaussianFlare(np.max(y), 9000., 500000.)
    # fl5.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 8000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    curve.add_flares(bg, fl1, fl2)
    # curve.add_hyper_prior({'amp': {'loc': (stats.uniform, {'loc': 0.,
    #                                                        'scale': 5.}),
    #                                'scale': (stats.uniform, {'loc': 1.,
    #                                                          'scale': 100.})}
    #                        })

    # Fit with MCMC
    data = (curve, t, y, yerr)
    ndim = 3 + curve.size
    nwalkers = 64
    print "Sampling with {} dim and {} workers".format(ndim, nwalkers)

    # # Generating initial vector
    # p0 = []
    # for i in range(nwalkers):
    #     p0_ = [dic[par] for fl, dic in zip(curve._flares, curve.from_prior) for
    #            par in fl._parnames]
    #     p0.append(list(0.1 * np.random.rand(3)) + p0_)

    # Alternative generating p0:
    p0 = list()
    i = 0
    while i < nwalkers:
        p = list()
        for fl, dic in zip(curve._flares, curve.from_prior):
            for par in fl._parnames:
                p.append(dic[par])
        curve.p = p
        print curve._is_sorted()
        if curve._is_sorted():
            p0.append(list(0.1 * np.random.rand(3)) + p)
            i += 1

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob_gp, args=data)

    print("Running burn-in")
    p0, lnp, _ = sampler.run_mcmc(p0, 200)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    sampler.reset()

    p = p0[np.argmax(lnp)]
    print("Running second burn-in from point {}".format(p))
    p0 = [p + 1e-2 * np.random.randn(ndim) for i in xrange(nwalkers)]
    p0, _, _ = sampler.run_mcmc(p0, 500)
    sampler.reset()

    print("Running production")
    p0, _, _ = sampler.run_mcmc(p0, 500)

    # Plot the samples in data space.
    print("Making plots")
    samples = sampler.flatchain
    import corner
    corner.corner(sampler.flatchain[::10, :])

    x = np.linspace(0., t[-1], 500)
    plt.figure()
    plt.errorbar(t, y, yerr=yerr, fmt=".k", capsize=0)
    for i, s in enumerate(samples[np.random.randint(len(samples), size=24)]):
        print "Drawing sample {}".format(i)
        # gp = george.GP(np.exp(s[0]) * george.kernels.Matern32Kernel(np.exp(s[1])))
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.Matern32Kernel(np.exp(s[1])) +
                       george.kernels.WhiteKernel(np.exp(s[2])))
        gp.compute(t, yerr)
        curve.p = s[3:]
        m = gp.sample_conditional(y - curve(t), x) + curve(x)
        plt.plot(x, m, color="#4682b4", alpha=0.25)
        # plt.plot(x, fl1(x) + fl2(x), color='red', alpha=0.25)
        plt.plot(x, gp.sample_conditional(y - curve(t), x), color='red', alpha=0.25)
