import emcee
import numpy as np
from scipy.stats import norm


def model(p, x):
    """
    Model of time flare lags relative to highest frequency flares.

    dt = a * nu ** (-1/k) + b

    :param p:
    :param x:
        Array of frequencies in ascending order.
    :return:
    """
    return p[0] * x ** (-1./p[1]) + p[2]


def lnlike(p, x, y, yerr):
    """
    Log of likelihood.
    :param p:
    :param x:
    :param y:
        Time lags between flares at different frequencies relative to highest.
    :param yerr:
    """
    return (-0.5 * np.log(2. * np.pi * yerr ** 2) - (y - model(p, x)) ** 2. /
            (2. * yerr ** 2)).sum()
    # return (-0.5 * np.log(2. * np.pi * (yerr ** 2 + np.exp(p[3]))) - (y - model(p, x)) ** 2.
    #         / (2. * (yerr ** 2 + np.exp(p[3])))).sum()


def lnprior(p):
    if not 0 < p[0] < 1000:
        return -np.inf
    if not 0 < p[1] < 5:
        return -np.inf
    if not -20 < p[2] < 0:
        return -np.inf
    # if not -3 < p[3] < 10:
    #     return -np.inf
    return 0.


def lnprior_push(p):
    if not 0 < p[0] < 1000:
        return -np.inf
    if not -20 < p[2] < 0:
        return -np.inf
    return norm.logpdf(p[1], 0.9, 0.44)


def lnprior_func(p):
    if not 0 < p[0] < 1000:
        return -np.inf
    if not -20 < p[2] < 0:
        return -np.inf
    if not p[1] > 0:
        return -np.inf
    return logp_k(p[1])


def prior_func(k):
    return 6.904088 * (k*abs(k-1))**(-1.) * np.sqrt(37.**((k-1.)/k)*((k-1)/k*np.log(37.)-1)-
                                                    5.**((k-1)/k)*((k-1)/k*np.log(5.)-1))


def logp_k(k):
    return np.log(prior_func(k))


def prior_0(x):
    if 0 < x < 1000:
        return 0.001
    else:
        return 0


def prior_2(x):
    if -20 < x < 0:
        return 1./20
    else:
        return 0


def prior_1_push(x):
    return np.exp(norm.logpdf(x, 0.9, 0.44))


def prior_1_func(x):
    return prior_func(x)


def prior_1(x):
    if 0 < x < 5:
        return 0.2
    else:
        return 0


def lnpost_push(p, x, y, yerr):
    lp = lnprior_push(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(p, x, y, yerr)


def lnpost_func(p, x, y, yerr):
    lp = lnprior_func(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(p, x, y, yerr)


def lnpost(p, x, y, yerr):
    lp = lnprior(p)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(p, x, y, yerr)


def kfit(x, y, yerr, nwalkers=100, n_burnin=3000, n_samples=1000, thin=10):
    """
    Function that obtaines samples from posterior distribution of ``a``, ``k``
    parameters of model:

    ``y = a * x ** (-1./k) + b``

    :param x:
        Iterable of arguments (frequencies).
    :param y:
        Time lags between flares at different frequencies relative to highest.
    :param yerr:
        Uncertainty estimates for ``y``.
    :return:
        Samples from posterior distribution of ``a, k, b``.
    """
    x = np.array(x)
    y = np.array(y)
    yerr = np.array(yerr)

    sampler = emcee.EnsembleSampler(nwalkers, 3, lnpost, args=(x, y, yerr))
    p0 = [100., 1., -3]
    p0 = emcee.utils.sample_ball(p0, [10, 0.1, 1.], size=nwalkers)

    print("Running burn-in")
    p0, lnp, _ = sampler.run_mcmc(p0, n_burnin)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    p0 = p0[np.argmax(lnp)]
    print("Running second burn-in from point {}".format(p0))
    p0 = emcee.utils.sample_ball(p0, [10, 0.1, 1.], size=nwalkers)
    sampler.reset()
    p0, _, _ = sampler.run_mcmc(p0, n_burnin)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    sampler.reset()

    print("Running production")
    p0, lnp, _ = sampler.run_mcmc(p0, n_samples * thin, thin=thin)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    p_map = p0[np.argmax(lnp)]

    samples = sampler.flatchain[::thin, :]

    return samples, p_map


if __name__ == '__main__':
    ndim = 3
    x = [5., 8., 15., 37]
    # x = [2.64, 4.85, 8.35, 10.45, 14.60, 15.00, 23.05, 32.00, 43.05, 86.24, 142.33]
    # 5-37, 8-37, 15-37, 37-37
    y = [20., 11., 6., 0.]
    # y_dccf = np.array([203, 145, 100, 91, 68, 63, 45, 51, 44, 25, 0.])
    # y_gp = [207.1, 128.8, 92.4, 80.6, 66.3, 64.2, 33.8, 28.6, 37.7, 24.7, 0]
    # yerr_dccf = [29, 20, 22, 22, 23, 48, 31, 32, 36, 42, 40]
    # yerr_gp = len(y_gp) * [0.4]

    # FIXME: Check the error of 37 time maximum
    yerr = np.array([4., 3., 3., 2.]) / 2.
    samples, p_map = kfit(x, y, yerr, n_burnin=300, n_samples=1000)
    # import corner
    import matplotlib.pyplot as plt
    # figc, axes = plt.subplots(nrows=ndim, ncols=ndim)
    # figc.set_size_inches(13.5, 13.5)
    # figc = corner.corner(samples, fig=figc,
    #                      labels=[r'$a$', r'$k$', r'$t_{37-\inf}$'],
    #                      show_titles=True, title_kwargs={'fontsize': 16},
    #                      label_kwargs={'fontsize': 16},
    #                      quantiles=[0.16, 0.5, 0.84])
    # figc.show()
    figc, axes = plt.subplots(nrows=ndim, ncols=ndim)
    figc.set_size_inches(13.5, 13.5)
    figc = corner(samples, fig=figc, labels=[r'$a$', r'$k$', r'$t_{37-\inf}$'],
                  show_titles=True, title_kwargs={'fontsize': 16},
                  label_kwargs={'fontsize': 16}, quantiles=[0.16, 0.5, 0.84],
                  priors=(prior_0, prior_1_func, prior_2),
                  hist_kwargs={'normed':True})


    # fig = plt.figure()
    # plt.errorbar(x, y, yerr, fmt='.k', lw=1.5)
    # x_ = np.linspace(x[0]-1, x[-1]+1, 200)
    # plt.plot(x_, model(p_map, x_), color='r', label=r'MAP a={:.0f} k={:.2f} '
    #                                                 'b={:.1f}'.format(*p_map[:3]),
    #          lw=2)
    # plt.xlabel("Frequency, [GHz]")
    # plt.ylabel("Time lag of maximum relative to 37GHz, days")
    # plt.ylim([min(y)-0.2*yerr[np.argmin(y)],
    #           max(y)+0.2*yerr[np.argmax(y)]])
    # plt.legend()
    # for i, p in enumerate(samples[np.random.randint(len(samples),
    #                                                 size=24)]):
    #     print "Drawing sample {}".format(i)
    #     plt.plot(x_, model(p, x_), color="#4682b4", alpha=0.25)
    # fig.show()


