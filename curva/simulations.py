import numpy as np
import emcee
import matplotlib.pyplot as plt
import george
from scipy import stats
from flares import ConstBackground, GaussianFlare, Curve, ExpFlare
from statistic import lnprob_gp


if __name__ == "__main__":
    # Generate fake data
    t = np.linspace(0, 3000, 500)
    t = np.array(sorted(np.random.choice(t, size=350, replace=False)))
    fl1 = ExpFlare(10., 1000., 100., 200.)
    fl2 = ExpFlare(14., 2000., 150., 250.)
    fl3 = ExpFlare(5., 2300., 100., 200.)
    bg = ConstBackground(2.)
    curve = Curve()
    curve.add_flares(bg, fl1, fl2, fl3)
    noise = np.random.normal(0, 0.3, size=350)
    gp = george.GP(0.3 * george.kernels.Matern32Kernel(300))
    corr_noise = gp.sample(t)
    y = curve(t) + noise + corr_noise
    yerr = 0.3 * np.ones(len(t))
    plt.figure()
    plt.errorbar(t, y, yerr, fmt='.k')


    # Initialize Curve object
    curve = Curve()
    bg = ConstBackground(np.mean(y))
    bg.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 5.})})
    fl1 = ExpFlare(np.max(y), 800., 50., 100.)
    fl1.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 20.}),
                   'loc': (stats.uniform, {'loc': 500., 'scale': 1000.}),
                   'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
                   'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    fl2 = ExpFlare(np.max(y), 2000., 50., 100.)
    fl2.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 20.}),
                   'loc': (stats.uniform, {'loc': 1500, 'scale': 600}),
                   'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
                   'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    fl3 = ExpFlare(np.max(y), 2300., 50., 100.)
    fl3.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 20.}),
                   'loc': (stats.uniform, {'loc': 2100., 'scale': t[-1]-2100}),
                   'sig21': (stats.uniform, {'loc': 0., 'scale': 1000.0}),
                   'sig22': (stats.uniform, {'loc': 0., 'scale': 1000.0})})
    # fl1 = GaussianFlare(np.max(y), 1000., 500000.)
    # fl1.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #               'loc': (stats.uniform, {'loc': 0., 'scale': 1700.}),
    #               'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl2 = GaussianFlare(np.max(y), 3000., 500000.)
    # fl2.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #               'loc': (stats.uniform, {'loc': 1700., 'scale': 4000.}),
    #               'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl3 = GaussianFlare(np.max(y), 5000., 500000.)
    # fl3.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 4000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl4 = GaussianFlare(np.max(y), 7000., 500000.)
    # fl4.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 6000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    # fl5 = GaussianFlare(np.max(y), 9000., 500000.)
    # fl5.add_prior({'amp': (stats.uniform, {'loc': 0., 'scale': 100.}),
    #                'loc': (stats.uniform, {'loc': 8000., 'scale': 2000.}),
    #                'sig2': (stats.uniform, {'loc': 0., 'scale': 4000000.0})})
    curve.add_flares(bg, fl1, fl2, fl3)
    # curve.add_hyper_prior({'amp': {'loc': (stats.uniform, {'loc': 0.,
    #                                                        'scale': 5.}),
    #                                'scale': (stats.uniform, {'loc': 1.,
    #                                                          'scale': 100.})}
    #                        })

    # Fit with MCMC
    data = (curve, t, y, yerr)
    initial = [0., 0., 0.] + curve.p
    ndim = len(initial)
    nwalkers = 32
    print "Sampling with {} dim and {} workers".format(ndim, nwalkers)
    p0 = []
    for i in range(nwalkers):
        p0_ = [dic[par] for fl, dic in zip(curve._flares, curve.from_prior) for
               par in fl._parnames]
        p0.append(list(0.1 * np.random.rand(3)) + p0_)
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob_gp, args=data)

    print("Running burn-in")
    p0, lnp, _ = sampler.run_mcmc(p0, 200)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    sampler.reset()

    p = p0[np.argmax(lnp)]
    print("Running second burn-in from point {}".format(p))
    p0 = [p + 1e-1 * np.random.randn(ndim) for i in xrange(nwalkers)]
    p0, _, _ = sampler.run_mcmc(p0, 400)
    sampler.reset()

    print("Running production")
    p0, _, _ = sampler.run_mcmc(p0, 500)

    # Plot the samples in data space.
    print("Making plots")
    samples = sampler.flatchain
    import corner
    corner.corner(sampler.flatchain[::10, :])

    x = np.linspace(t[0], t[-1], 500)
    plt.figure()
    plt.errorbar(t, y, yerr=yerr, fmt=".k", capsize=0)
    for i, s in enumerate(samples[np.random.randint(len(samples), size=24)]):
        print "Drawing sample {}".format(i)
        # gp = george.GP(np.exp(s[0]) * george.kernels.Matern32Kernel(np.exp(s[1])))
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.Matern32Kernel(np.exp(s[1])) +
                       george.kernels.WhiteKernel(np.exp(s[2])))
        gp.compute(t, yerr)
        curve.p = s[3:]
        m = gp.sample_conditional(y - curve(t), x) + curve(x)
        plt.plot(x, m, color="#4682b4", alpha=0.25)
        plt.plot(x, curve(x))
