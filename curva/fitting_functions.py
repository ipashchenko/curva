import datetime
import numpy as np
import emcee
import matplotlib.pyplot as plt
import george
import corner
from statistic import lnprob_gp_only, lnprob_gp_only_rq


# TODO: Make on function with optional kernels.
def fit_gp(t, y, yerr, n_samples=12, n_grid=300, nwalkers=32, n_burnin_1=200,
           n_burnin_2=200, n_sample=500, outname_corner_plot=None,
           outname_fit_plot=None):
    """
    Functions that fit GP (with sum of two kernels - Mattern 3/2 & White noise)
    to light curve.

    :param t:
        Iterable of instances of `datetime.datetime`.
    :param y:
        Iterable of fluxes.
    :param yerr:
        Iterable of uncertainty estimates.
    :param n_samples: (optional)
        Number of samples (realizations of GP fit of the light curve) from
        posterior to return. (default: ``12``)
    :param n_grid: (optional)
        Number of sampling points on each of the returned GP realization. Each
        of ``n_samples`` returned realization will have ``n_grid`` points
        constructed by ``np.linspace(t_min, t_max, n_grid)``. (default: ``300``)
    :param nwalkers: (optional)
        Number of ``emcee`` walkers to use. (default: ``32``).
    :param n_burnin_1: (optional)
        Number of first burn-in steps to make. (default: ``200``)
    :param n_burnin_2: (optional)
        Number of second burn-in steps to make. (default: ``200``)
    :param n_sample: (optional)
        Number of production run steps to make. (default: ``500``)
    :param outname_corner_plot: (optional)
        Name of the file to save the plot of posterior distribution of GP
        hyperparameters (amplitude, scale, white noise dispersion). If ``None``
        then use ``corner.png``. (default: ``None``)
    :param outname_fit_plot: (optional)
        Name of the file to save plot of fitted GP (w/o white noise part).
        If ``None`` then use ``fit_gp_only.png``. (default: ``None``)

    :return:
        ``samples``, ``t``, where ``samples`` - list of ``n_samples``
        realizations (each has length ``n_grid``) from GP with hyperparameters
        sampled from posterior, ``t`` - array of ``datetime.datetime`` instances
        (with length ``n_grid``) at which GP is sampled.

    :note:
        Samples can be plotted as ``[plt.plot(t, sample) for sample in
        samples]``.

        Distribution of times of maximum for samples can be obtained as follows:
        ``[t[np.argmax(sample)] for sample in samples]``

        Provided we fitted flare at two frequencies and have ``samples1, t1`` &
        ``samples2, t2`` output for both frequencies the distribution of time
        lag of flare maximum betwee two frequencies could be obtained as
        follows:
        ``[t1[np.argmax(sample1)] - t2[np.argmax(sample2)] for sample1, sample2
        in zip(samples1, samples2)]``

    """
    # Original datetime
    t_dt = t[:]
    delta_t = 0
    # Change to integer days
    t = np.array([ti.toordinal() for ti in t])
    plt.figure()
    plt.errorbar(t, y, yerr, fmt='.k')
    xy = plt.ginput(n=2, timeout=-1, show_clicks=True, mouse_add=1,
                    mouse_pop=3, mouse_stop=2)
    # 2 points define the time window to zoom in
    t1 = xy[0][0]
    t2 = xy[1][0]
    plt.xlim(t1, t2)
    plt.draw()
    xy = plt.ginput(n=2, timeout=-1, show_clicks=True, mouse_add=1,
                    mouse_pop=3, mouse_stop=2)
    # 2 points define the time window to fit
    t1 = xy[0][0]
    t2 = xy[1][0]
    plt.close()
    delta_t += t1
    indx = np.logical_and(t > t1, t < t2)
    t = t[indx]
    t_dt0 = np.array(t_dt)[indx][0]
    t0 = t[0]
    t -= t0
    y = y[indx]
    yerr = yerr[indx]

    # Setting up sampler
    ndim = 3
    nwalkers = nwalkers
    data = (t, y, yerr)
    print "Sampling with {} dims and {} workers".format(ndim, nwalkers)

    p0 = list()
    i = 0
    while i < nwalkers:
        p0.append(list(0.1 * np.random.rand(ndim)))
        i += 1

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob_gp_only, args=data)

    print("Running burn-in")
    p0, lnp, _ = sampler.run_mcmc(p0, n_burnin_1)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    sampler.reset()

    p = p0[np.argmax(lnp)]
    print("Running second burn-in from point {}".format(p))
    p0 = [p + 1e-2 * np.random.randn(ndim) for i in xrange(nwalkers)]
    p0, _, _ = sampler.run_mcmc(p0, n_burnin_2)
    sampler.reset()

    print("Running production")
    p0, _, _ = sampler.run_mcmc(p0, n_sample)

    samples = sampler.flatchain
    fig, axes = plt.subplots(nrows=ndim, ncols=ndim)
    fig.set_size_inches(13.5, 13.5)
    corner.corner(sampler.flatchain[::10, :], fig=fig,
                  labels=[r'$\log a$', r'$\log \tau$', '$\log \sigma^2$'],
                  show_titles=True, title_kwargs={'fontsize': 16},
                  quantiles=[0.16, 0.5, 0.84], label_kwargs={'fontsize': 16})
    if outname_corner_plot is None:
        outname_corner_plot = 'corner.png'
    fig.savefig(outname_corner_plot, bbox_inches='tight', dpi=200)

    x = np.linspace(0., t[-1], 500)
    fig, axes = plt.subplots(1, 1)
    t_orig = t_dt0 + np.array([datetime.timedelta(days=ti) for ti in x])
    axes.errorbar(np.array(t_dt)[indx], y, yerr=yerr, fmt=".k", capsize=0)
    axes.set_ylabel('Flux')
    for i, s in enumerate(samples[np.random.randint(len(samples), size=24)]):
        print "Plotting sample {}".format(i)
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.Matern32Kernel(np.exp(s[1])))
        gp.compute(t, yerr)
        m = gp.sample_conditional(y, x)
        axes.plot(t_orig, m, color="#4682b4", alpha=0.25)
    plt.gcf().autofmt_xdate()
    if outname_fit_plot is None:
        outname_fit_plot = 'fit_gp_only.png'
    fig.savefig(outname_fit_plot, bbox_inches='tight', dpi=200)

    t_grid = np.linspace(0., t[-1], n_grid)
    curve_samples = list()
    for i, s in enumerate(samples[np.random.randint(len(samples),
                                                    size=n_samples)]):
        print "Drawing sample {}".format(i)
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.Matern32Kernel(np.exp(s[1])))
        gp.compute(t, yerr)
        m = gp.sample_conditional(y, t_grid)
        curve_samples.append(m)

    t_orig = t_dt0 + np.array([datetime.timedelta(days=ti) for ti in t_grid])

    return curve_samples, t_orig


def fit_gp_rq(t, y, yerr, n_samples=12, n_grid=300, nwalkers=32, n_burnin_1=200,
              n_burnin_2=200, n_sample=500, outname_corner_plot=None,
              outname_fit_plot=None):
    """
    Functions that fit GP (sum of two kernels - Rstional Quadratic & White
    noise) to light curve.

    :param t:
        Iterable of instances of `datetime.datetime`.
    :param y:
        Iterable of fluxes.
    :param yerr:
        Iterable of uncertainty estimates.
    :param n_samples: (optional)
        Number of samples (realizations of GP fit of the light curve) from
        posterior to return. (default: ``12``)
    :param n_grid: (optional)
        Number of sampling points on each of the returned GP realization. Each
        of ``n_samples`` returned realization will have ``n_grid`` points
        constructed by ``np.linspace(t_min, t_max, n_grid)``. (default: ``300``)
    :param nwalkers: (optional)
        Number of ``emcee`` walkers to use. (default: ``32``).
    :param n_burnin_1: (optional)
        Number of first burn-in steps to make. (default: ``200``)
    :param n_burnin_2: (optional)
        Number of second burn-in steps to make. (default: ``200``)
    :param n_sample: (optional)
        Number of production run steps to make. (default: ``500``)
    :param outname_corner_plot: (optional)
        Name of the file to save the plot of posterior distribution of GP
        hyperparameters (amplitude, scale, white noise dispersion). If ``None``
        then use ``corner.png``. (default: ``None``)
    :param outname_fit_plot: (optional)
        Name of the file to save plot of fitted GP (w/o white noise part).
        If ``None`` then use ``fit_gp_only.png``. (default: ``None``)

    :return:
        ``samples``, ``t``, where ``samples`` - list of ``n_samples``
        realizations (each has length ``n_grid``) from GP with hyperparameters
        sampled from posterior, ``t`` - array of ``datetime.datetime`` instances
        (with length ``n_grid``) at which GP is sampled.

    :note:
        Samples can be plotted as ``[plt.plot(t, sample) for sample in
        samples]``.

        Distribution of times of maximum for samples can be obtained as follows:
        ``[t[np.argmax(sample)] for sample in samples]``

        Provided we fitted flare at two frequencies and have ``samples1, t1`` &
        ``samples2, t2`` output for both frequencies the distribution of time
        lag of flare maximum betwee two frequencies could be obtained as
        follows:
        ``[t1[np.argmax(sample1)] - t2[np.argmax(sample2)] for sample1, sample2
        in zip(samples1, samples2)]``

    """
    # Original datetime
    t_dt = t[:]
    delta_t = 0
    # Change to integer days
    t = np.array([ti.toordinal() for ti in t])
    plt.figure()
    plt.errorbar(t, y, yerr, fmt='.k')
    xy = plt.ginput(n=2, timeout=-1, show_clicks=True, mouse_add=1,
                    mouse_pop=3, mouse_stop=2)
    # 2 points define the time window to zoom in
    t1 = xy[0][0]
    t2 = xy[1][0]
    plt.xlim(t1, t2)
    plt.draw()
    xy = plt.ginput(n=2, timeout=-1, show_clicks=True, mouse_add=1,
                    mouse_pop=3, mouse_stop=2)
    # 2 points define the time window to fit
    t1 = xy[0][0]
    t2 = xy[1][0]
    plt.close()
    delta_t += t1
    indx = np.logical_and(t > t1, t < t2)
    t = t[indx]
    t_dt0 = np.array(t_dt)[indx][0]
    t0 = t[0]
    t -= t0
    y = y[indx]
    yerr = yerr[indx]

    # Setting up sampler
    ndim = 4
    nwalkers = nwalkers
    data = (t, y, yerr)
    print "Sampling with {} dims and {} workers".format(ndim, nwalkers)

    p0 = list()
    i = 0
    while i < nwalkers:
        p0.append(list(0.1 * np.random.rand(ndim)))
        i += 1

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob_gp_only_rq,
                                    args=data)

    print("Running burn-in")
    p0, lnp, _ = sampler.run_mcmc(p0, n_burnin_1)
    print "Acceptance fraction: ", sampler.acceptance_fraction
    sampler.reset()

    p = p0[np.argmax(lnp)]
    print("Running second burn-in from point {}".format(p))
    p0 = [p + 1e-2 * np.random.randn(ndim) for i in xrange(nwalkers)]
    p0, _, _ = sampler.run_mcmc(p0, n_burnin_2)
    sampler.reset()

    print("Running production")
    p0, _, _ = sampler.run_mcmc(p0, n_sample)

    samples = sampler.flatchain
    fig, axes = plt.subplots(nrows=ndim, ncols=ndim)
    fig.set_size_inches(13.5, 13.5)
    corner.corner(sampler.flatchain[::10, :], fig=fig,
                  labels=[r'$\log a$', r'$\log \tau$', r'$\log \alpha$',
                          '$\log \sigma^2$'], show_titles=True,
                  title_kwargs={'fontsize': 16}, label_kwargs={'fontsize': 16},
                  quantiles=[0.16, 0.5, 0.84])
    plt.gcf().autofmt_xdate()
    if outname_corner_plot is None:
        outname_corner_plot = 'corner.png'
    fig.savefig(outname_corner_plot, bbox_inches='tight', dpi=200)

    x = np.linspace(0., t[-1], 500)
    fig, axes = plt.subplots(1, 1)
    t_orig = t_dt0 + np.array([datetime.timedelta(days=ti) for ti in x])
    axes.errorbar(np.array(t_dt)[indx], y, yerr=yerr, fmt=".k", capsize=0)
    axes.set_ylabel('Flux')
    for i, s in enumerate(samples[np.random.randint(len(samples), size=24)]):
        print "Platting sample {}".format(i)
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.RationalQuadraticKernel(np.exp(s[2]),
                                                              np.exp(s[1])))
        gp.compute(t, yerr)
        m = gp.sample_conditional(y, x)
        axes.plot(t_orig, m, color="#4682b4", alpha=0.25)
    plt.gcf().autofmt_xdate()
    if outname_fit_plot is None:
        outname_fit_plot = 'fit_gp_only.png'
    fig.savefig(outname_fit_plot, bbox_inches='tight', dpi=200)

    t_grid = np.linspace(0., t[-1], n_grid)
    curve_samples = list()
    for i, s in enumerate(samples[np.random.randint(len(samples),
                                                    size=n_samples)]):
        print "Drawing sample {}".format(i)
        gp = george.GP(np.exp(s[0]) *
                       george.kernels.RationalQuadraticKernel(np.exp(s[2]),
                                                              np.exp(s[1])))
        gp.compute(t, yerr)
        m = gp.sample_conditional(y, t_grid)
        curve_samples.append(m)

    t_orig = t_dt0 + np.array([datetime.timedelta(days=ti) for ti in t_grid])

    return curve_samples, t_orig


if __name__ == "__main__":
    # Load data
    import pickle
    with open('/home/ilya/code/gp_curves/curva/out.pkl', 'rb') as fo:
        data = pickle.load(fo)
    t1, y1, yerr1, t2, y2, yerr2 = data
    y1 = np.array(y1, dtype='float')
    y2 = np.array(y2, dtype='float')
    yerr1 = np.array(yerr1, dtype='float')
    yerr2 = np.array(yerr2, dtype='float')
    # t, y, yerr - iterables of ``datetime.datetime`` & floats.
    t = t2
    # y = y2
    y = y2 - np.mean(y2)
    yerr = yerr2

    samples, t = fit_gp_rq(t, y, yerr, n_samples=10, n_grid=1000)
    # tmax = [t[np.argmax(sample)] for sample in samples]
    # from scipy.stats import scoreatpercentile
    # t18, t50, t84 = scoreatpercentile([t.toordinal() for t in tmax],
    #                                   [16, 50, 84])
    # print t50 - t18, t84 - t50
    # print np.std([t.toordinal() for t in tmax])
