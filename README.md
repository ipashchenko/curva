# Curva #

Python code for fitting light curves with gaussian process.

### What you can do with curva? ###

* Fit a gaussian process to your light curve.
* Fit your light curve with any combinations of flare models and correlated noise model.